#!/usr/bin/env python

# DutchPirate-threaded.py is available under the BSD License.

# This program has been written in a cooperative effort by:
# Jeroen van der Ham (UvA)
# Cosmin Dumitru (UvA)
# Ralph Koning (UvA)

import Queue
import pygeoip
from dns import resolver,reversename
import threading
import socket
import bulkwhois.cymru
import os
from collections import defaultdict

GEOIP = pygeoip.GeoIP('GeoIP.dat')

class Results(object):
    def __init__(self,filename):
        self.filename = filename
        self.results = {}
        self.total_cc_ips = 0
        self.cc_asn_dict = {}
        self.total_ips = 0
        
    
    def add_result(self, asn, cc, desc, ip):
        if self.results.has_key(asn):
            self.results[asn][2].append(ip)
        else:
            self.results[asn] = (cc,desc,[ip])
    
    def get_asn_name(self,asn):
        return self.results[asn][1].split()[0]
        
    def calc_stats(self,cc):
        cc_asn_dict = {}
        total_cc_ips = 0.0
        for asn in self.results:
            if self.results[asn][0] == cc:
                cc_asn_dict[asn] = 0
                total_cc_ips += len(self.results[asn][2])
        for asn in cc_asn_dict:
            cc_asn_dict[asn] = len(self.results[asn][2])/total_cc_ips * 100
        self.total_cc_ips = total_cc_ips
        self.cc_asn_dict = cc_asn_dict
        
def convert6to4(ip):
    if not ":" in ip:
        return ip
    if not ip.startswith("2002:"):
        return None
    s = ip.split(":")
    first,last = s[1:3]
    if len(first) < 4:
        first = "0"+first
    if len(last) < 4:
        last = "0"+last
    return "%s.%s.%s.%s" % (int(first[0:2],16),int(first[2:4],16),int(last[0:2],16),int(last[2:4],16))

def zero_pad_list(values,length):
    diff = length - len(values)
    for x in range(diff):
        values.append(0)
    return values
        
def calc_average(values,length):
    """ Return the average of the values in the list. """
    values = zero_pad_list(values,length)
    return sum(values) / float(length)

def calc_sd(values,length):
    """ Computer the standard deviation of the values in
        the list.
    """
    values = zero_pad_list(values,length)
    mean = calc_average(values,length)
    differences = [(value - mean)**2 for value in values]
    return calc_average(differences,length) ** 0.5

PREFIX = "nl-full2/"
results = []
bulkwhois = bulkwhois.cymru.BulkWhoisCymru()
for filename in os.listdir(PREFIX):
    print filename
    if not filename.startswith("processed"):
        result = Results(filename)
        ips = sorted(set( [line.strip().split(",")[0] for line in open(PREFIX+filename)]))
        ips = [convert6to4(ip) for ip in ips]
        ips = set(ips)
        ips.discard(None)
        result.total_ips = len(ips)
        cymru = bulkwhois.lookup_ips(ips)
        for ip in ips:
            try:
                country = GEOIP.country_code_by_name(ip)
                result.add_result(cymru[ip]['asn'],country, cymru[ip]['as_name'], ip)
            except socket.gaierror:
                result.add_result(cymru[ip]['asn'],cymru[ip]['cc'], cymru[ip]['as_name'], ip)
        result.calc_stats('NL')
        if result.total_cc_ips:
            results.append(result)

resultfile = open("cymru-%s.txt" % PREFIX[:-1],'w')
asn_results=defaultdict(list)
for result in results:
    resultfile.write(result.filename+"\n")
    resultfile.write("Total IPs: %s\n" % result.total_ips)
    resultfile.write("Total Dutch IPs: %s\n" % int(result.total_cc_ips))
    for asn in result.cc_asn_dict:
        resultfile.write("%s: %.2f%% " % (result.get_asn_name(asn),result.cc_asn_dict[asn]))
        asn_results[result.get_asn_name(asn)].append(result.cc_asn_dict[asn])
    resultfile.write("\n")
    resultfile.write(80*"-"+"\n")


resultfile.write(80*"="+"\n")
nr_results = len(results)

for asn in asn_results:
    resultfile.write("%s: %.2f%% (%.2f%%)" % (asn, calc_average(asn_results[asn],nr_results), calc_sd(asn_results[asn],nr_results)))