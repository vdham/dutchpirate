from collections import defaultdict
import pygeoip
from dns import resolver,reversename
import socket
import os

GEOIP = pygeoip.GeoIP('GeoIP.dat')

ziggo_total = 0
xs4all_total = 0
nl_total = 0

isps = defaultdict(int)
total_dutch_ips= 0

PREFIX = "jer-nl-logs/"
def convert6to4(ip):
    s = ip.split(":")
    first,last = s[1:3]
    if len(first) < 4:
        first = "0"+first
    if len(last) < 4:
        last = "0"+last
    return "%s.%s.%s.%s" % (int(first[0:2],16),int(first[2:4],16),int(last[0:2],16),int(last[2:4],16))

timeouts = []

for filename in os.listdir(PREFIX):
    if not filename.startswith("processed"):
        print filename
        ips = sorted(set( [line.strip().split(",")[0] for line in open(PREFIX+filename)]))
        out_file = PREFIX+"processed"+filename
        out = open(out_file, "w")

        for ip in ips:
            if ip.startswith("2002:"):
                ip = convert6to4(ip)
            dutch_ips = 0
            try:
                country = GEOIP.country_code_by_name(ip)
                #country, isp, ip, full_reverse
                if ip in timeouts:
                    out_string = "%s,,%s," % (country , ip)
                else:
                    addr = reversename.from_address(ip)
                    print "resolving %s" % ip

                    full_resolve = str(resolver.query(addr,"PTR")[0])
                    isp = full_resolve.split(".")[-3]
                    if country == "NL":
                        isps[isp] +=1
                        dutch_ips  += 1
                    out_string = "%s,%s,%s,%s" % ( country ,isp,ip, full_resolve )
            except resolver.NXDOMAIN:
                # print "no reverse found for %s" % ip
                out_string = "%s,,%s," % (country , ip)
            except resolver.Timeout:
                # print "following timed out %s" % ip
                out_string = "%s,,%s," % (country , ip)
                timeouts.append(ip)
            except socket.gaierror:
                # print "resolving failed for %s" % ip
                out_string = "%s,,%s," % (country , ip)
                timeouts.append(ip)
            except:
                print "FAILURE! %s" % ip 
                out_string = "%s,,%s," % (country , ip)
            out.write(out_string+"\n")
        out.close()
        total_dutch_ips +=dutch_ips
        print "Done with %s" % filename
        # print "ziggo fraction %s" % str(isps["ziggo"]*100 / dutch_ips)
        # print "xs4all fraction %s" % str(isps["xs4all"]*100 / dutch_ips)
        print "%s: %s" % (total_dutch_ips, isps)
