from collections import defaultdict
import os


total_dutch_ips = 0
total_ziggo = []
total_xs4all = []
isps = defaultdict(int)


PREFIX = "jer-logs/"
f = open("totalstats-%s.txt"%PREFIX[:-1],'w')

for filename in os.listdir(PREFIX):
    if filename.startswith("processed"):
        isps = defaultdict(int)
        dutch_ips = 0
        ziggo = 0
        xs4all = 0

        lines = [line.strip() for line in open(PREFIX+filename)]

        for l in lines:
            line = l.split(',')
            country = line[0]
            isp = line[1]
            if "NL" in country:
                isps[isp] +=1
                dutch_ips +=1
        print "%s: %s" % (dutch_ips, filename)
        ziggo = float(isps['ziggo']) *100 / dutch_ips
        xs4all= float(isps['xs4all']) *100 / dutch_ips
        total_dutch_ips +=dutch_ips

        total_ziggo.append(ziggo)
        total_xs4all.append(xs4all)
        
        # leave out processed part:
        f.write(filename[9:])
        f.write("\ntotal NL %s\n" % dutch_ips)
        f.write("ziggo  %.2f%% (%s)"% (ziggo,isps['ziggo']),)
        f.write(" xs4all %.2f%% (%s)\n" % (xs4all,isps['xs4all']),)
        f.write(str(isps))
        f.write("\n")
        f.write("===\n")


def average(values):
    """ Return the average of the values in the list. """
    return sum(values) / float(len(values))

def standardDeviation(values):
    """ Computer the standard deviation of the values in
        the list.
    """
    mean = average(values)
    differences = [(value - mean)**2 for value in values]
    return average(differences) ** 0.5

print "total dutch: %s" % (total_dutch_ips)
print "total ziggo: %.2f%% (%.2f%%)\n" % (average(total_ziggo),standardDeviation(total_ziggo))
print "total xs4all %.2f%% (%.2f%%)\n" % (average(total_xs4all),standardDeviation(total_xs4all))

f.write("total dutch: %s\n" % (total_dutch_ips))
f.write("total ziggo: %.2f%% (%.2f%%)\n" % (average(total_ziggo),standardDeviation(total_ziggo)))
f.write("total xs4all %.2f%% (%.2f%%)\n" % (average(total_xs4all),standardDeviation(total_xs4all)))
f.close()